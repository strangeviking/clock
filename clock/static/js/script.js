function Clock (){
  return {

    init: function(options) {
      if (!options){
        return;
      }
      this.clk = options.customContext;
      this.radius = options.customRadius;
      this.offset = options.customOffset;
    },

    drawClock: function() {
	    this.drawFace();
	    this.drawTime();
    },

    drawFace: function () {
      var grad;

      this.clk.beginPath();
      this.clk.arc(0, 0, this.radius, 0, 2*Math.PI);
      this.clk.fillStyle = 'white';
      this.clk.fill();

      grad = this.clk.createRadialGradient(0,0, this.radius*0.95, 0,0, this.radius*1.05);
      grad.addColorStop(0, '#333');
      grad.addColorStop(0.5, 'white');
      grad.addColorStop(1, '#333');
      this.clk.strokeStyle = grad;
      this.clk.lineWidth = this.radius*0.1;
      this.clk.stroke();

      this.clk.beginPath();
      this.clk.arc(0, 0, this.radius*0.1, 0, 2*Math.PI);
      this.clk.fillStyle = '#333';
      this.clk.fill();
    },

    drawTime: function () {
      var now = new Date();
      var hour = now.getUTCHours();
      var minute = now.getUTCMinutes();
      var second = now.getUTCSeconds();
      //hour
      hour=(hour + this.offset)%12;
      hour=(hour*Math.PI/6)+(minute*Math.PI/(6*60))+(second*Math.PI/(360*60));
      this.drawHand(hour, this.radius*0.5, this.radius*0.07);
      //minute
      minute=(minute*Math.PI/30)+(second*Math.PI/(30*60));
      this.drawHand(minute, this.radius*0.8, this.radius*0.07);
      // second
      second=(second*Math.PI/30);
      this.drawHand(second, this.radius*0.9, this.radius*0.02);
    },

    drawHand: function (pos, length, width) {
      this.clk.beginPath();
      this.clk.lineWidth = width;
      this.clk.lineCap = "round";
      this.clk.moveTo(0,0);
      this.clk.rotate(pos);
      this.clk.lineTo(0, -length);
      this.clk.stroke();
      this.clk.rotate(-pos);
    }
  }

}

function drawClockSetup(customCanvas, customOffset){
  var canvas = customCanvas || document.getElementById('utc');
  var translateSize = canvas.height / 2;
  var clk = canvas.getContext("2d");
  clk.translate(translateSize, translateSize);
  var radius = translateSize * 0.90;
  var offset = customOffset || 0;

  var clockInstance = new Clock();
  clockInstance.init({
    customContext: clk,
    customRadius: radius,
    customOffset: offset
  });
  clockInstance.drawClock();
  setInterval(function(){
    clockInstance.drawClock();
  }, 1000);
};
drawClockSetup();

function newClock() {

  var tzlist = document.getElementById('tzlist');
  var tzoffset=tzlist.value;
  var clock_name=tzlist.options[tzlist.selectedIndex].getAttribute('name');

  var h1 = document.createElement('h1');
  h1.innerText = tzoffset;

  var div = document.createElement('div');
  div.className = 'clock';

  var newCanvas = document.createElement('canvas');
  newCanvas.setAttribute('id', clock_name);
  newCanvas.setAttribute('width', 200);
  newCanvas.setAttribute('height', 200);

  div.appendChild(h1);
  div.appendChild(newCanvas);

  var clocksContainer = document.querySelector('.clocks');
  clocksContainer.appendChild(div);

  drawClockSetup(newCanvas, tzoffset);
}